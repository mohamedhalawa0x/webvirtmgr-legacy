from django.db import models
from django.conf import settings
from django_countries.fields import CountryField
from django.core.validators import RegexValidator, EmailValidator
from django.contrib.auth.models import (
    BaseUserManager, AbstractBaseUser
)
from .utils import code_generator
from django.db.models.signals import post_save
from django.core.mail import send_mail


class MyUserManager(BaseUserManager):
    def create_user(self, email, first_name, last_name, date_of_birth, country, address, state_region, phone_number, postal_code, password=None):
        """
        Creates and saves a User with the given email, date of
        birth and password.
        """
        if not email:
            raise ValueError('Users must have an email address')

        user = self.model(
            email=self.normalize_email(email),
            first_name=first_name,
            last_name=last_name,
            date_of_birth=date_of_birth,
            country=country,
            address=address,
            state_region=state_region,
            phone_number=phone_number,
            postal_code=postal_code,

        )

        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_superuser(self, email, first_name, last_name, date_of_birth, country, address, state_region, phone_number, postal_code, password):
        """
        Creates and saves a superuser with the given email, date of
        birth and password.
        """
        user = self.create_user(
            email,
            first_name,
            last_name,
            date_of_birth,
            country,
            address,
            state_region,
            phone_number,
            postal_code,
            password=password,
        )
        user.is_admin = True
        user.save(using=self._db)
        return user


ALPHABETS_REGEX = r'^[a-zA-Z]*$'
PHONE_REGEX = r'^\+?1?\d{9,15}$'
UPLOAD_TO = "profilepictures/"


class MyUser(AbstractBaseUser):
    # Email Address
    email = models.EmailField(
        verbose_name='email address',
        max_length=255,
        unique=True,
        validators=[
            EmailValidator(
                message="Not a Valid Email! , use something like example@example.example ",
                code="invalid_email"
            )
        ]
    )
    # first name
    first_name = models.CharField(
        max_length=128,
        unique=False,
        verbose_name="first name",
        validators=[
            RegexValidator(
                regex=ALPHABETS_REGEX,
                message="first name must be Alphabets 'a-z' only.",
                code="invalid_name"
            )
        ])
    # last name
    last_name = models.CharField(
        max_length=128,
        unique=False,
        verbose_name="last name",
        validators=[
            RegexValidator(
                regex=ALPHABETS_REGEX,
                message="last name must be Alphabets 'a-z' only.",
                code="invalid_name"
            )
        ])
    # Country
    country = CountryField()

    # Postal Code
    postal_code = models.CharField(
        verbose_name="Postal Code", max_length=7, blank=False, null=False, )

    # state/region
    state_region = models.CharField(
        verbose_name="State / Region", max_length=128, blank=True, null=True)

    # Street Address
    address = models.CharField(
        max_length=255, blank=False, null=False, verbose_name="Street Address")

    # Phone number
    phone_number = models.CharField(
        max_length=15,
        validators=[
            RegexValidator(
                regex=PHONE_REGEX,
                message="Phone number must be entered in the format: '+999999999'. Up to 15 digits allowed.",
                code="invalid_number"
            )
        ])

    # Profile Picture Field
    # image = models.ImageField(upload_to=UPLOAD_TO, blank=True)

    # Date of birth
    date_of_birth = models.DateField()

    # is active
    is_active = models.BooleanField(default=True)

    # is admin
    is_admin = models.BooleanField(default=False)

    objects = MyUserManager()

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['first_name', 'last_name', 'date_of_birth',
                       'country', 'address', 'state_region', 'phone_number', 'postal_code', ]

    def __str__(self):
        return self.email

    # Get User Name
    def get_fullname(self):
        return self.first_name+" "+self.last_name

    def has_perm(self, perm, obj=None):
        "Does the user have a specific permission?"
        # Simplest possible answer: Yes, always
        return True

    def has_module_perms(self, app_label):
        "Does the user have permissions to view the app `app_label`?"
        # Simplest possible answer: Yes, always
        return True

    @property
    def is_staff(self):
        "Is the user a member of staff?"
        # Simplest possible answer: All admins are staff
        return self.is_admin


class ActivationProfile(models.Model):
    user = models.OneToOneField(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    key = models.CharField(max_length=120)
    expired = models.BooleanField(default=False)

    def save(self, *args, **kwargs):
        self.key = code_generator()
        super(ActivationProfile, self).save(*args, **kwargs)


def post_save_activation_reciever(sender, instance, created, *args, **kwargs):
    if created:
        try:
            url = "http://127.0.0.1:8000/accounts/activate/" + instance.key
            # send email
            subject = " VLX - Account Activation"
            msg = "Kindly click the following hyperlink:\n"+url
            efrom = "virtlabx@gmail.com"
            eto = instance.user.email
            send_mail(subject,msg,efrom,[eto],fail_silently=False,)
        except:
            pass


post_save.connect(post_save_activation_reciever, sender=ActivationProfile)


def post_save_user_model_reciever(sender, instance, created, *args, **kwargs):
    if created:
        try:
            ActivationProfile.objects.create(user=instance)
        except:
            pass


post_save.connect(post_save_user_model_reciever,
                  sender=settings.AUTH_USER_MODEL)
